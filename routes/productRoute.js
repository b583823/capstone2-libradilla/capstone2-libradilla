const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");


router.get("/allProduct", auth.verify, (req,res)=>{
	productController.getAllProduct().then(resultFromController=>res.send(resultFromController));
})
router.get("/activeProduct", auth.verify, (req,res)=>{
	productController.getActiveProduct().then(resultFromController=>res.send(resultFromController));
})
router.get("/:productId", auth.verify, (req,res)=>{
	productController.getProduct(req.params.productId).then(resultFromController=>res.send(resultFromController));
})

router.post("/addProduct",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	productController.addProduct(req.body).then(resultFromController=>res.send(resultFromController));
	}
	else{
		return res.send("You are not allowed to this page.")
	}
})

router.put("/:productId",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productController.updateProduct(req.params.productId, req.body).then(resultFromController=>res.send(resultFromController));
	}
	else{
		return res.send("You are not allowed to this page.")
	}
})
router.patch("/:productId",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
	productController.archiveProduct(req.params.productId).then(resultFromController=>res.send(resultFromController));
	}
	else{
		return res.send("You are not allowed to this page.")
	}
})

router.get("/getAllOrders", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userController.getOrders().then(resultFromController=> res.send(resultFromController))	
	}
	else{
		res.send("You're not allowed to access this page")
	}
})




module.exports = router;