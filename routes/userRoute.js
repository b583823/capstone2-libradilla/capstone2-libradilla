const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");


router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})
router.patch("/:id/setAsAdmin", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userController.setAdmin(req.params.id).then(resultFromController=> res.send(resultFromController))
		
	}
	else{
		res.send("You're not allowed to access this page")
	}
})
router.post("/createOrder", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	let data= {
		userId: userData.id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	if(userData.isAdmin){
		res.send("You're not allowed to access this page")	
	}
	else{
		userController.createOrder(data).then(resultFromController=> res.send(resultFromController))
	}
})

router.get("/userOrders", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	userController.orders(userData.id).then(resultFromController=> res.send(resultFromController))
})

router.get("/getAllOrders", auth.verify, (req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		userController.getOrders().then(resultFromController=> res.send(resultFromController))	
	}
	else{
		res.send("You're not allowed to access this page")
	}
})
module.exports = router;