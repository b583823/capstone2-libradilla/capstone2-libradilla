const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const productRoute = require("./routes/productRoute");
const userRoute = require("./routes/userRoute");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@zuittbootcamp.e1kfs3u.mongodb.net/ecommerce-app?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/products", productRoute);

app.listen(process.env.PORT || port, ()=>{
	console.log(`API is now online on port ${process.env.PORT|| port}`)
});