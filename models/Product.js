const mongoose = require("mongoose");
const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product Name is required."]
	},
	description: {
		type: String,
		required: [true, "Product description is required."]
	},
	price:{
		type: Number,
		required: [true, "Product price is required."]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	prodCreatedOn:{
		type: Date,
		default: new Date()
	},
	stocks:{
		type: Number,
		default: 1
	},
	userOrders: [
		{
			user: {
				type: String
			},
			productPurchasedOn:{
				type: Date,
				default: new Date()
			},
			quantitySold:{
				type: Number,
				default: 1
			}
		}
	]

})
module.exports = mongoose.model("Product", productSchema);