const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [ 
				{
						totalAmount: {
							type: Number
						},
						quantity: {
							type: Number,
							default: 1
						},
						purchasedOn: {
							type: Date,
							default: new Date()
						},
						productId: {
							type: String,
						}
				}
			]

})

module.exports = mongoose.model("User", userSchema);