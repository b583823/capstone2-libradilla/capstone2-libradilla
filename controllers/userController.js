const User = require("../models/User")
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product")

module.exports.registerUser =(reqBody) =>{
	return User.find({email: reqBody.email}).then(result=>{
		if(result.length<=0){
			let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
			})
			return newUser.save().then((user, error)=>{
				if(error){
					return false;
				}
				else{
					console.log(user);
					return true;
				}
			})
		}
		else{
			return "Email already exist."
		}
	})
	
	
	}
	


module.exports.loginUser = (reqBody) =>{
	return User.findOne({email: reqBody.email}).then(result=>{
		
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else{
				return false;
			}
		}
	})
}

module.exports.setAdmin = (userId) =>{
	let updateUser = {isAdmin:true}
	console.log(userId)

	return User.findByIdAndUpdate(userId, updateUser).then((updateUser, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.createOrder = async (data) =>{
	let productPrice = await Product.findById(data.productId).then(product=>product.price)
	let productStock = await Product.findById(data.productId).then(product=>product.stocks)
	let productStatus = await Product.findById(data.productId).then(product=>product.isActive)
	if(productStock >= 0 && productStatus){
		let isUserUpdated = await User.findById(data.userId).then(user=>{
			user.orders.push({
				productId: data.productId,
				quantity: data.quantity,
				totalAmount: productPrice * data.quantity
			})
			return user.save().then((orders, error)=>{
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})
		let isProductUpdated = await Product.findById(data.productId).then(product=>{
			product.stocks-=data.quantity
			product.userOrders.push({
				user: data.userId,
				quantitySold: data.quantity
			})
			return product.save().then((userOrders,error)=>{
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
	}
	else{
		return "Product Unavailable"
	}
}

module.exports.orders = (userId) =>{
	return User.findById(userId).then(result=>{
		return result.orders
	})
}

module.exports.getOrders = () =>{
	return User.find({},{
		password:0,
		_Id:0,
		createdOn:0,
		__v:0

	}).then(result=>result)
}
