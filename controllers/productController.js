const Product = require("../models/Product");
const auth = require("../auth");

module.exports.getAllProduct = () =>{
	return Product.find({}).then(result=>result);
}
module.exports.getActiveProduct = () =>{
	return Product.find({isActive:true}).then(result=>result);
}
module.exports.getProduct = (productId) =>{
	return Product.findById(productId).then(result=>result);
}

module.exports.addProduct = (reqBody) =>{
	let newProduct = new Product({
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	})
	return newProduct.save().then((course,error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.updateProduct = (productId,reqBody) =>{
	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((updatedProduct, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}
module.exports.archiveProduct = (productId) =>{
	let updatedProduct = {isActive:false}
	return Product.findByIdAndUpdate(productId, updatedProduct).then((updatedProduct, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

